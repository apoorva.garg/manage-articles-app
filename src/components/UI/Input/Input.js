import React from 'react'

import './Input.css';

const input = (props) => {

    let inputElement = null;

    switch (props.elementtype) {
        case ('input'):
            inputElement = <input
                type="text"
                className="InputElement"
                {...props}
                value={props.value} />;
            break;
        case ('textarea'):
            inputElement = <textarea
                className="InputElement"
                {...props}
                value={props.value} />;
            break;
        default:
            inputElement = <input
                type={props.type}
                className="InputElement"
                {...props}
                value={props.value}
            />;
    }

    return (
        <div className="Input">
            <label className="Label">{props.label}</label>
            {inputElement}
        </div>
    )
}

export default input