import React, { Component } from 'react';

import './Modal.css';
import Backdrop from '../Backdrop/Backdrop';
import Button from '../Button/Button';

class Modal extends Component {

    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.show !== this.props.show || nextProps.children !== this.props.children;
    }

    render() {
        return (
            <React.Fragment>
                <Backdrop show={this.props.show} clicked={this.props.modalClosed} />
                <div
                    className="Modal"
                    style={{
                        transform: this.props.show ? 'translateY(0)' : 'translateY(-100vh)',
                        opacity: this.props.show ? '1' : '0'
                    }}>
                    <header className="Modal-header">
                        <p className="Modal-title">{this.props.title}</p>
                        <span>
                            <Button btnType="reset_button" clicked={this.props.modalClosed}>Close</Button>
                        </span>
                    </header>
                    {this.props.children}
                </div>
            </React.Fragment>
        )
    }
}

export default Modal;