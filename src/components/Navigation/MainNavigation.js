import React from 'react';
import { NavLink } from 'react-router-dom';

import './MainNavigation.css';

const mainNavigation = (props) => {
    return (
        <header className="main-navigation">
            <div className="main-navigation-title">
                <h1>Articles Manager</h1>
            </div>
            <nav className="main-navigation-items">
                <ul>
                    <li><NavLink to="/add-articles">Add Articles</NavLink></li>
                    <li><NavLink to="/published-articles">Published Articles</NavLink></li>
                </ul>
            </nav>
        </header>
    )
}

export default mainNavigation