import React, { useEffect } from 'react'
import { connect } from 'react-redux';

import './PublishedArticles.css';

import * as actions from '../../../store/actions/articles';
import Spinner from '../../UI/Spinner/Spinner';
import Article from './Article/Article'


const PublishedArtciles = props => {

    useEffect(() => {
        props.fetchArticles()
    }, [])

    let showArticles = <Spinner />;

    if (!props.loading) {
        showArticles = props.articles.map(art => {
            const key = `${art.id}_${art.title}_${art.body}`

            return <Article
                key={key}
                title={art.title}
                body={art.body} />
        })
    }

    return (
        <div>
            <h2 style={{ textAlign: 'center' }}>Published Articles</h2>
            {showArticles}
        </div>
    )
}

const mapStateToProps = state => {
    return {
        articles: state.ArticleReducer.articles,
        loading: state.ArticleReducer.loading,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        fetchArticles: () => dispatch(actions.getArticles())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PublishedArtciles)