import React from 'react'

import './Article.css'

const article = ({ title, body }) => {
    return (
        <div className="article-main">
            <p className="article-title">{title}</p>
            <p className="article-body">{body}</p>
        </div>
    )
}

export default article