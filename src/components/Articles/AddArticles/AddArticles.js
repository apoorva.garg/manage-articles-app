import React, { useState } from 'react'

import './AddArticles.css';

import AddArticle from  './AddArticle/AddArticle';

const AddArticles = props => {

    const articles = {
        title: '',
        body: '',
        id: ''
    }

    const [openArticleModal, setArticleModal] = useState(false)

    const openAddArticleHandler = () => {
        setArticleModal(true);
    }

    const cancelAddArticleHandler = () => {
        setArticleModal(false);
    }

    return (
        <div className="add-articles-header">
            <div className="add-articles-add" onClick={openAddArticleHandler}>
                <div className="fa fa-plus" style={{ color: '#2b5f8d', marginRight: '15px' }}></div>
                     Add Articles
            </div>
            <small>Added Articles can be seen in Published Articles tab.</small>
            {
                openArticleModal && <AddArticle openArticleModal={openArticleModal} cancelAddArticle={cancelAddArticleHandler} articles={articles}/>
            }
        </div>
    )
}


export default AddArticles;