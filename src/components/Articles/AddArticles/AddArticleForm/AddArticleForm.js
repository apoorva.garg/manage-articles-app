import React, { useState } from 'react'
import './AddArticleForm.css';


import Input from '../../../UI/Input/Input';
import Button from '../../../UI/Button/Button';

const AddArticleForm = props => {

    const [title, setTitle] = useState(props.articles.title || '')
    const [body, setBody] = useState(props.articles.body || '')
    const [id, setId] = useState(props.articles.id || '')

    return (
        <React.Fragment>
            <main className="form-main">
                <Input label="Enter Title" key="title" elementtype="text" placeholder="Enter Title" value={title} onChange={event => setTitle(event.target.value)} />
                <Input label="Enter Body" key="body" elementtype="textarea" placeholder="Enter Body" rows="4" value={body} onChange={event => setBody(event.target.value)} />
            </main>
            <footer className="form-footer">
                {id == '' && <Button btnType="save_button" clicked={() => props.minimizeHanlder(title, body)}>Minimize</Button>}
                <Button btnType="save_button" clicked={() => props.addArticleHandler(title, body, id)}>Submit</Button>
            </footer>
        </React.Fragment>
    )
}

export default AddArticleForm