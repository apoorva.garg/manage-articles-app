import React from 'react'
import { connect } from 'react-redux';
import { v4 as uuidv4 } from 'uuid';
import { withRouter } from 'react-router-dom';

import './AddArticle.css';


import Modal from '../../../UI/Modal/Modal';
import AddArticleForm from '../AddArticleForm/AddArticleForm';
import * as actions from '../../../../store/actions/articles';
import * as taskBarActions from '../../../../store/actions/taskbar';

const AddArticle = props => {

    const minimizeHanlder = (title, body) => {
        const newMinimizedDialog = {
            title: title,
            body: body,
            id: uuidv4()
        }
        props.minimizeForm(newMinimizedDialog);
        props.cancelAddArticle();
    }

    const addArticleHandler = (title, body, id) => {
        const payload = {
            title: title,
            body: body
        }
        props.addArticle(payload)
        if (id !== '') {
            props.removeFromTaskbar(id)
        }
        props.cancelAddArticle()
        props.history.replace('/add-articles')
    }

    return (
        <div>
            <Modal show={props.openArticleModal} modalClosed={props.cancelAddArticle} title="Article Form">
                <AddArticleForm addArticleHandler={addArticleHandler} minimizeHanlder={minimizeHanlder} articles={props.articles} />
            </Modal>
        </div>
    )
}

const mapDispatchToProps = dispatch => {
    return {
        addArticle: (article) => dispatch(actions.addArticle(article)),
        minimizeForm: (newMinimizedDialog) => dispatch(taskBarActions.minimizeArticleForm(newMinimizedDialog)),
        removeFromTaskbar: (id) => dispatch(taskBarActions.removeFromMinimizeArticleForm(id))
    }
}

export default withRouter(connect(null, mapDispatchToProps)(AddArticle));