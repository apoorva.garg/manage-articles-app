import React, { useState } from 'react'

import './TaskbarDialogs.css'

import AddArticle from '../../Articles/AddArticles/AddArticle/AddArticle';


const TaskbarDialogs = props => {

    const [openDialogModal, setOpenDialogModal] = useState(false)

    const openDialogModalHandler = () => {
        setOpenDialogModal(true);
    }

    const cancelDialogModalHandler = () => {
        setOpenDialogModal(false);
    }

    return (
        <React.Fragment>
            <div className="taskbarDialogs" onClick={openDialogModalHandler}>
                A ({props.index})
            </div>
            {
                openDialogModal && <AddArticle openArticleModal={openDialogModal} cancelAddArticle={cancelDialogModalHandler} articles={props.articles}/>
            }
        </React.Fragment>
    )
}

export default TaskbarDialogs