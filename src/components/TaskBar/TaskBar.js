import React, { useState } from 'react'
import { connect } from 'react-redux';

import './TaskBar.css';

import TaskBarDialogs from './TaskbarDialogs/TaskbarDialogs'

const Taskbar = props => {

    const [openTaskbar, setOpenTaskbar] = useState(false)

    const openTaskBarHandler = () => {
        setOpenTaskbar(!openTaskbar)
    }

    return (
        <div className="taskbar">
            {openTaskbar && (
                <div className="taskbar-content">
                    {
                        props.minimized_dialogs.map((pendingArticles, index) => (
                            <TaskBarDialogs
                                key={pendingArticles.id}
                                articles={pendingArticles}
                                index={index + 1}
                            />
                        ))
                    }
                </div>
            )
            }
            <div className="taskbar-main" onClick={openTaskBarHandler}>
                <p className="taskbar-title">TaskBar ({props.minimized_dialogs.length} pending)</p>
            </div>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        minimized_dialogs: state.TaskbarReducer.minimized_dialogs
    }
}

export default connect(mapStateToProps)(Taskbar);