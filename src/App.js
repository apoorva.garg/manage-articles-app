import React from 'react';
import { connect } from 'react-redux';
import { Route, Redirect, Switch } from 'react-router-dom'
import './App.css';

import MainNavigation from './components/Navigation/MainNavigation';
import Taskbar from './components/TaskBar/TaskBar';
import AddArticles from './components/Articles/AddArticles/AddArticles';
import PublishedArticles from './components/Articles/PublishedArticles/PublishedArticles';

const app = props => {
  console.log(props)
  return (
    <React.Fragment>
      <MainNavigation />
      {props.minimizeForm && props.minimized_dialogs.length > 0 && <Taskbar />}
      <main className="main-content">
        <Switch>
          <Redirect from="/" to="/add-articles" exact />
          <Route path="/add-articles" component={AddArticles} />
          <Route path="/published-articles" component={PublishedArticles} />
        </Switch>
      </main>
    </React.Fragment>
  );
}

const mapStateToProps = state => {
  return {
    minimizeForm: state.TaskbarReducer.minimize,
    minimized_dialogs: state.TaskbarReducer.minimized_dialogs
  }
}

export default connect(mapStateToProps)(app);
