import * as actionTypes from './actionTypes';
import axios from '../../axios-articles';


export const getArticlesStart = () => {
    return {
        type: actionTypes.GET_ARTICLES_START
    }
}

export const getArticlesSuccess = (articles) => {
    return {
        type: actionTypes.GET_ARTICLES_SUCCESS,
        articles: articles
    }
}

export const getArticlesFail = (error) => {
    return {
        type: actionTypes.GET_ARTICLES_FAIL,
        error: error
    }
}


export const addArticleStart = () => {
    return {
        type: actionTypes.ADD_ARTICLES_START
    }
}

export const addArticleSuccess = (article) => {
    return {
        type: actionTypes.ADD_ARTICLES_SUCCESS,
        article: article
    }
}

export const addArticleFail = (error) => {
    return {
        type: actionTypes.ADD_ARTICLES_FAIL,
        error: error
    }
}

export const getArticles = () => {
    return dispatch => {
        dispatch(getArticlesStart());
        axios.get('/posts')
            .then(res => {
                const articles = res.data.slice(0, 3);
                dispatch(getArticlesSuccess(articles))
            })
            .catch(err => {
                dispatch(getArticlesFail(err));
            })
    }
}

export const addArticle = (article) => {
    return dispatch => {
        dispatch(addArticleStart());
        axios.post('/posts', article)
            .then(res => {
                dispatch(addArticleSuccess(res.data))
            })
            .catch(err => {
                dispatch(addArticleFail(err))
            })
    }
}