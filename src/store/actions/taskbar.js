import * as actionTypes from './actionTypes';

export const minimizeArticleForm = (newMinimizedDialog) => {
    return {
        type: actionTypes.MINIMIZE_ARTICLE_FORM,
        minimized_dialog: newMinimizedDialog
    }
}

export const removeFromMinimizeArticleForm = (id) => {
    return {
        type: actionTypes.REMOVE_MINIMIZE_ARTICLE_FORM,
        id: id
    }
}