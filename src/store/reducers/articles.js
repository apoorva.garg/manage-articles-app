import * as actionTypes from '../actions/actionTypes';

const initialState = {
    articles: [],
    newArticles: [],
    loading: false
}

const getArticlesStart = (state, action) => {
    return {
        ...state,
        loading: true
    }
}

const getArticlesSuccess = (state, action) => {
    const articles = [...action.articles, ...state.newArticles]
    const updatedState = {
        articles: articles,
        loading: false
    }
    return {
        ...state,
        ...updatedState
    }
}

const getArticlesFail = (state, action) => {
    return {
        ...state,
        loading: false
    }
}

const addArticleStart = (state, action) => {
    return {
        ...state,
        loading: true
    }
}

const addArticleSuccess = (state, action) => {
    const article = action.article
    const newArticles = [...state.newArticles, article]
    const updatedState = {
        articles: state.articles,
        newArticles: newArticles,
        loading: false
    }
    return {
        ...state,
        ...updatedState
    }
}

const addArticleFail = (state, action) => {
    return {
        ...state,
        loading: false
    }
}


const articles = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GET_ARTICLES_START: return getArticlesStart(state, action)
        case actionTypes.GET_ARTICLES_SUCCESS: return getArticlesSuccess(state, action)
        case actionTypes.GET_ARTICLES_FAIL: return getArticlesFail(state, action)
        case actionTypes.ADD_ARTICLES_START: return addArticleStart(state, action)
        case actionTypes.ADD_ARTICLES_SUCCESS: return addArticleSuccess(state, action)
        case actionTypes.ADD_ARTICLES_FAIL: return addArticleFail(state, action)
        default:
            return state;
    }
}

export default articles