import * as actionTypes from '../actions/actionTypes';

const initialState = {
    minimize: false,
    minimized_dialogs: []
}


const minimizeArticleForm = (state, action) => {
    const all_minimized_dialogs = [...state.minimized_dialogs, action.minimized_dialog]
    const updatedState = {
        minimized_dialogs: all_minimized_dialogs,
        minimize: true
    }
    return {
        ...state,
        ...updatedState
    }
}

const removeFromMinimizeArticleForm = (state, action) => {
    const id = action.id;
    const dialogs = [...state.minimized_dialogs]
    const newDialogs = dialogs.filter(dialog => (
        dialog.id !== id
    ))
    const updatedState = {
        minimized_dialogs: newDialogs,
        minimize: true
    }
    return {
        ...state,
        ...updatedState
    }
}

const taskbar = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.MINIMIZE_ARTICLE_FORM: return minimizeArticleForm(state, action)
        case actionTypes.REMOVE_MINIMIZE_ARTICLE_FORM: return removeFromMinimizeArticleForm(state, action)
        default:
            return state;
    }
}

export default taskbar