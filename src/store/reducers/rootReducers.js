import {combineReducers} from 'redux'

import ArticleReducer from './articles'
import TaskbarReducer from './taskbar';

const rootReducer = combineReducers({
    ArticleReducer,
    TaskbarReducer
})

export default rootReducer